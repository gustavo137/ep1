#include <bits/stdc++.h>
#include <fstream>           //library to read filles

#include "candidatos.hpp"
#include "urna.hpp"
#include "pessoa.hpp"
#include "eleitor.hpp"

using namespace std;

int main (){
string file_1 = "data/consulta_cand_2018_BR.csv";
string file_2 = "data/consulta_cand_2018_DF.csv";
int option = 1;
int votacaoRealizada = 0;
Urna novaUrna;

novaUrna.setNumeroCandidatos(file_1);//passo necessário para registrar candidatos
novaUrna.setNumeroCandidatos(file_2);
novaUrna.registraCandidato(file_1);//registro de candidatos
novaUrna.registraCandidato(file_2);

do {

cout << "\n __________________Urna_Eletronica_2018___________________\n";
cout << "|                                                         |\n";
cout << "| 1-Iniciar nova votação                                  |\n";
cout << "| 2-Apresentar resultados                                 |\n";
cout << "| 3-Buscar Candidato                                      |\n";
cout << "| 4-Mostrar relatorio de votos                            |\n";
cout << "|                                                         |\n";
cout << "| 0-Encerrar urna                                         |\n";
cout << "|_________________________________________________________|\n\n";

cin >> option;
cout << "\n";

if (option == 0){
cout << "Urna encerrada\n";
} else

if (option == 1)
{

novaUrna.iniciaVotacao();
votacaoRealizada = 1;

}else
if (option == 2 && votacaoRealizada == 1)
{
novaUrna.apresentaResultados();
}else
if (option == 3 && votacaoRealizada == 1)
{
string busca;
cin >> busca;
novaUrna.buscaCandidato(busca);
}else
if (option == 4 && votacaoRealizada == 1)
{
novaUrna.lista_votos();
}else
{
cout << "\n\nOpcao indisponivel, primeiro realize uma votacao";
cout << "\n(1) Continuar";
cout << "\n(0) Encerrar Urna\n";
cout << "Digito : ";
cin >> option;
cout << "\n";
}

}while (option != 0);
return 0;
}

