#include "voto.hpp"
#include <string>

Voto::Voto(){
        string nome_eleitor = "";
        nome_deputado_federal = "";
        nome_deputado_distrital = "";
	nome_governador = "";
        nome_senador = "";
        nome_presidente = "";
}

Voto::Voto(string nome, string deputado_federal,string deputado_distrital,string governador,string senador,string presidente){

	set_nome_eleitor(nome);
        set_nome_deputado_federal(deputado_federal);
        set_nome_deputado_distrital(deputado_distrital);
	set_nome_governador(governador);
        set_nome_senador(senador);
        set_nome_presidente(presidente);

}

string Voto::get_nome_eleitor()
{
return nome_eleitor;
}
string Voto::get_nome_deputado_federal()
{
return nome_deputado_federal;
}
string Voto::get_nome_deputado_distrital()
{
return nome_deputado_distrital;
}
string Voto::get_nome_governador()
{
return nome_governador;
}

string Voto::get_nome_senador()
{
return nome_senador;
}
string Voto::get_nome_presidente()
{
return nome_presidente;
}

void Voto::set_nome_eleitor(string eleitor){
this ->nome_eleitor = eleitor;
}
void Voto::set_nome_deputado_federal(string deputado_federal){
this -> nome_deputado_federal = deputado_federal;
}

void Voto::set_nome_deputado_distrital(string deputado_distrital){
this -> nome_deputado_distrital = deputado_distrital;
}

void Voto::set_nome_governador(string governador){
this -> nome_governador = governador;
}

void Voto::set_nome_senador(string senador){
this -> nome_senador = senador;
}

void Voto::set_nome_presidente(string presidente){
this -> nome_presidente = presidente;
}
