#include "candidatos.hpp"
#include <string>
using namespace  std;

//Construtor padrão
Candidato::Candidato(){
        nm_ue = "";
        ds_cargo = "";
        nr_candidato = "";
	nm_urna_candidato = "";
        nm_partido = "";
        votos = 0;
}

Candidato::Candidato(string estado, string cargo, string numero,
string nome, string partido, long long int votos){

        setEstado(estado);
        setCargo(cargo);
        setNumero(numero);
        setNomeUrna(nome);
        setPartido(partido);
	setVoto();
}

string  Candidato::getEstado(){
return nm_ue;
}
string Candidato::getCargo(){
return ds_cargo;
}
string Candidato::getNumero(){
return nr_candidato;
}
string Candidato::getNomeUrna(){
return nm_urna_candidato;
}
string Candidato::getPartido(){
return nm_partido;
}
long long int Candidato::getVotos(){
return votos;
}

void Candidato::setEstado(string estado) {
this ->nm_ue = estado;
}

void Candidato::setCargo(string cargo) {
this ->ds_cargo = cargo;
}

void Candidato::setNumero(string numero) {
this ->nr_candidato = numero;
}

void Candidato::setNomeUrna(string nome) {
this ->nm_urna_candidato = nome;
}

void Candidato::setPartido(string partido) {
this ->nm_partido = partido;
}

void Candidato::setVoto() {
this ->votos = votos+1;
}

