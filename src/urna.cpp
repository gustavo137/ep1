#include <bits/stdc++.h>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <string>

#include "candidatos.hpp"
#include "urna.hpp"
#include "voto.hpp"

using namespace std;

Urna::Urna (){

}

Urna::~Urna(){
cout << "Urna encerrada\n";
}

int Urna::getNumeroCandidatos(){
  return this-> numero_de_candidatos;
}

void Urna::setNumeroCandidatos(string path_to_file)
{

  Candidato candidatoRegistrado[26];
  int numero_de_registros;

  for (int i = 1; i <= 26 ; i++){
    int nc = -1;
    string nr_candidato;
    int linhas=0;
    string line;

    fstream inputFile;
    inputFile.open (path_to_file.c_str());

    int option;
    option = i;
    option++;
    while (getline (inputFile, line))
      {
      linhas++;
      if (linhas == option)
        {
        for (int i = 0 ; i < line.size(); i++)
          {
            if (linhas == 18+ option - 2 && line[i] != ';' && line[i] != '"')
            {
            nr_candidato += line[i];
            }
            if (line[i] == ';')
            {
            linhas++;
            int b = 3;
            }else
            int a = 0;
          }
        }
      if (nr_candidato != "")
      {
      nc++;
      }
    }
    inputFile.close();
    numero_de_registros = nc + option - 1;
  }
  Urna::numero_de_candidatos += numero_de_registros;
}

void Urna::registraCandidato(string path_to_file){

  Candidato candidatoRegistrado[Urna::numero_de_candidatos];

  for (int i = 1; i <= Urna::numero_de_candidatos ; i++)
  {
  int nc = -1;

    string nm_ue;              //estado ou unidade da federação (DISTRITO FEDERAL) linha 13 em diante
    string ds_cargo;           // GOVERNADOR, presidente e etc
    string nr_candidato;       // NUMERO do candidato
    string nm_urna_candidato;  //nome na urna
    string nm_partido;         // nome do partido

    int linhas=0;
    string line;

  fstream inputFile;
  inputFile.open (path_to_file.c_str());

  int option;
  option = i;
  option++;
  while (getline (inputFile, line))
  {
  linhas++;
    if (linhas == option)
    {
      for (int i = 0 ; i < line.size(); i++)
      {
        if (linhas == 14 + option - 2 && line[i] != ';' && line[i] != '"')
        {
        nm_ue += line[i];
        }
        if (linhas == 16 + option - 2 && line[i] != ';' && line[i] != '"')
        {
        ds_cargo += line[i];
        }
        if (linhas == 18+ option - 2 && line[i] != ';' && line[i] != '"')
        {
        nr_candidato += line[i];
        }
        if (linhas == 20 + option - 2&& line[i] != ';' && line[i] != '"')
        {
        nm_urna_candidato += line[i];
        }
        if (linhas == 31+ option - 2 && line[i] != ';' && line[i] != '"')
        {
        nm_partido += line[i];
        }
        if (line[i] == ';')
        {
          linhas++;
	  int b = 3;
        }else
	  int a = 0;
      }
    }
  if (nr_candidato != ""){
      nc++;
      }
  }

  inputFile.close();

  int numero_de_registros = nc + option - 1;

  candidatoRegistrado[i-1].setEstado(nm_ue);
  candidatoRegistrado[i-1].setCargo(ds_cargo);
  candidatoRegistrado[i-1].setNumero(nr_candidato);
  candidatoRegistrado[i-1].setNomeUrna(nm_urna_candidato);
  candidatoRegistrado[i-1].setPartido(nm_partido);

  Urna::candidato_Registrado.push_back(candidatoRegistrado[i-1]);

  }
}

void Urna::iniciaVotacao()
  {

  int quantidade_de_eleitores;
  int quantidade_de_candidatos = Urna::numero_de_candidatos;
  int k = 0;
  int tipo_de_voto = 0;

  cout << "\nVotação iniciada \n";
  cout << quantidade_de_candidatos << " Candidatos Registrados\n";
  cout << "\n Informe a quantidade de eleitores da sessao: ";
  cin >> quantidade_de_eleitores;

  Voto voto_local[quantidade_de_eleitores];

  char nome_eleitor[100];
  char cpf_eleitor[100];

  for (int ii = 0; ii < quantidade_de_eleitores; ii++)
  {
    string deputado_federal,deputado_distrital,governador,senador,presidente;
    string nome_eleitor,cpf_eleitor,sobrenome_eleitor;
    cout << "\nDigite o seu nome e sobrenome :";
    cin >> nome_eleitor >> sobrenome_eleitor;

    voto_local[ii].set_nome_eleitor(nome_eleitor);

    cout << "\nDigite o seu CPF :";
    cin >> cpf_eleitor;

    int candidato_encontrado = 0;

    cout << "\ndigite (1) - Votar em um Dep.Federal\n";
    cout << "digite (2) - Votar em um branco\n";
    cout << "digite (3) - Anular o voto\n" << "Opcao:";
    cin >> tipo_de_voto;
    cout << "\n";
    if (tipo_de_voto == 2)
    {
    Urna::numero_de_votos_brancos++;
    }else
    if (tipo_de_voto == 3)
    {
    Urna::numero_de_votos_nulos++;
    }else
    if (tipo_de_voto == 1)
    {
      do
      {
        cout << "\nDigite o codigo de seu Dep.Federal (4 digitos): ";
        cin >> deputado_federal;
        do
        {
          if (deputado_federal.size() != 4 )
          {
          cout << "\nCodigo para deputado federal não encontrado, informe um codigo valido:\n";
          cin >> deputado_federal;
          }
          k = 0;
          for (int i = 0 ; i < Urna::candidato_Registrado.size(); i++)
          {
            if (deputado_federal == Urna::candidato_Registrado[i].getNumero())
            {
              cout << "\nCandidato encontrado : \n";
              cout <<"UF      : "<<Urna::candidato_Registrado[i].getEstado()  << "\n";
              cout <<"Cargo   : "<<Urna::candidato_Registrado[i].getCargo()   << "\n";
              cout <<"Numero  : "<<Urna::candidato_Registrado[i].getNumero()  << "\n";
              cout <<"Nome    : "<<Urna::candidato_Registrado[i].getNomeUrna()<< "\n";
              cout <<"Partido : "<<Urna::candidato_Registrado[i].getPartido() << "\n";

              candidato_encontrado = 1;

              k = i;
            }
          }
        }while( (deputado_federal.size() != 4));

      }while(candidato_encontrado == 0);

    voto_local[ii].set_nome_deputado_federal(candidato_Registrado[k].getNomeUrna());

    if (candidato_Registrado[k].getCargo() == "DEPUTADO FEDERAL" )
    {
    candidato_Registrado[k].setVoto();
    }

    if (candidato_Registrado[k].getVotos() > vencedor_deputado_federal.getVotos())
    {
    vencedor_deputado_federal = candidato_Registrado[k];
    }
  }
  candidato_encontrado = 0;

    cout << "\ndigite (1) - Votar em um Dep.Distrital\n";
    cout << "digite (2) - Votar em um branco\n";
    cout << "digite (3) - Anular o voto\n" << "Opcao:";
    cin >> tipo_de_voto;
    cout << "\n";

    if (tipo_de_voto == 2)
    {
    Urna::numero_de_votos_brancos++;
    }else

    if (tipo_de_voto == 3)
    {
    Urna::numero_de_votos_nulos++;
    }else

    if (tipo_de_voto == 1)
    {

    do
    {
      cout << "\nDigite o codigo de seu Dep.Distrital (5 digitos): ";
      cin >> deputado_distrital;

      do
      {
        if (deputado_distrital.size() != 5 )
        {
        cout << "\nCodigo para deputado não encontrado, informe um codigo valido:\n";
        cin >> deputado_distrital;
        }
        k = 0;
        for (int i = 0 ; i < Urna::candidato_Registrado.size(); i++)
        {
          if (deputado_distrital == Urna::candidato_Registrado[i].getNumero())
          {
	    cout << "\nCandidato encontrado : \n";
            cout <<"UF      : "<<Urna::candidato_Registrado[i].getEstado()  << "\n";
            cout <<"Cargo   : "<<Urna::candidato_Registrado[i].getCargo()   << "\n";
            cout <<"Numero  : "<<Urna::candidato_Registrado[i].getNumero()  << "\n";
            cout <<"Nome    : "<<Urna::candidato_Registrado[i].getNomeUrna()<< "\n";
            cout <<"Partido : "<<Urna::candidato_Registrado[i].getPartido() << "\n";

            candidato_encontrado = 1;
            k = i;
          }
        }
      }while( (deputado_distrital.size() != 5));

    }while(candidato_encontrado == 0);

  voto_local[ii].set_nome_deputado_distrital(candidato_Registrado[k].getNomeUrna());

  //if (candidato_Registrado[k].getCargo() == "DEPUTADO DISTRITAL" )
  candidato_Registrado[k].setVoto(); // voto efetivo no candidato

  if (candidato_Registrado[k].getVotos() > vencedor_deputado_distrital.getVotos())
  {
  vencedor_deputado_distrital = candidato_Registrado[k];
  }
  }
  candidato_encontrado = 0;

    cout << "\ndigite (1) - Votar em um Governador\n";
    cout << "digite (2) - Votar em um branco\n";
    cout << "digite (3) - Anular o voto\n" << "Opcao:";
    cin >> tipo_de_voto;
    cout << "\n";

    if (tipo_de_voto == 2)
    {
    Urna::numero_de_votos_brancos++;
    }else

    if (tipo_de_voto == 3)
    {
    Urna::numero_de_votos_nulos++;
    }else

    if (tipo_de_voto == 1)
    {

    do
    {
      cout << "\nDigite o codigo de seu Governador (2 digitos): ";
      cin >> governador;

      do
      {
        if (governador.size() != 2 )
        {
        cout << "\nCodigo para deputado não encontrado, informe um codigo valido:\n";
        cin >> governador;
        }
        k = 0;
        for (int i = 0 ; i < Urna::candidato_Registrado.size(); i++)
        {
          if (governador == Urna::candidato_Registrado[i].getNumero())
          {
            if (Urna::candidato_Registrado[i].getCargo() == "GOVERNADOR" )
            {
            cout << "\nCandidato encontrado : \n";
            cout <<"UF      : "<<Urna::candidato_Registrado[i].getEstado()  << "\n";
            cout <<"Cargo   : "<<Urna::candidato_Registrado[i].getCargo()   << "\n";
            cout <<"Numero  : "<<Urna::candidato_Registrado[i].getNumero()  << "\n";
            cout <<"Nome    : "<<Urna::candidato_Registrado[i].getNomeUrna()<< "\n";
            cout <<"Partido : "<<Urna::candidato_Registrado[i].getPartido() << "\n";
            candidato_encontrado = 1;
            k = i;
	    }
          }
        }
      }while( (governador.size() != 2));

    }while(candidato_encontrado == 0);

  voto_local[ii].set_nome_governador(candidato_Registrado[k].getNomeUrna());

  //if (candidato_Registrado[k].getCargo() == "GOVERNADOR" )
  candidato_Registrado[k].setVoto(); // voto efetivo no candidato

  if (candidato_Registrado[k].getVotos() > vencedor_governador.getVotos())
  {
  vencedor_governador = candidato_Registrado[k];
  }
  }
  candidato_encontrado = 0;

    cout << "\ndigite (1) - Votar em um Senador\n";
    cout << "digite (2) - Votar em um branco\n";
    cout << "digite (3) - Anular o voto\n" << "Opcao:";
    cin >> tipo_de_voto;
    cout << "\n";

    if (tipo_de_voto == 2)
    {
    Urna::numero_de_votos_brancos++;
    }else

    if (tipo_de_voto == 3)
    {
    Urna::numero_de_votos_nulos++;
    }else

    if (tipo_de_voto == 1)
    {

    do
    {
      cout << "\nDigite o codigo de seu Senador (3 digitos): ";
      cin >> senador;

      do
      {
        if (senador.size() != 3 )
        {
        cout << "\nCodigo para senador não encontrado, informe um codigo valido:\n";
        cin >> senador;
        }
        k = 0;
        for (int i = 0 ; i < Urna::candidato_Registrado.size(); i++)
        {
          if (senador == Urna::candidato_Registrado[i].getNumero())
          {
            if (Urna::candidato_Registrado[i].getCargo() == "SENADOR" )
            {
            cout << "\nCandidato encontrado : \n";
            cout <<"UF      : "<<Urna::candidato_Registrado[i].getEstado()  << "\n";
            cout <<"Cargo   : "<<Urna::candidato_Registrado[i].getCargo()   << "\n";
            cout <<"Numero  : "<<Urna::candidato_Registrado[i].getNumero()  << "\n";
            cout <<"Nome    : "<<Urna::candidato_Registrado[i].getNomeUrna()<< "\n";
            cout <<"Partido : "<<Urna::candidato_Registrado[i].getPartido() << "\n";
            candidato_encontrado = 1;
            k = i;
            }
          }
        }
      }while( (senador.size() != 3));

    }while(candidato_encontrado == 0);

  voto_local[ii].set_nome_senador(candidato_Registrado[k].getNomeUrna());

  //if (candidato_Registrado[k].getCargo() == "SENADOR" )
  //{
  candidato_Registrado[k].setVoto();
  //}

  if (candidato_Registrado[k].getVotos() > vencedor_senador.getVotos())
  {
  vencedor_senador = candidato_Registrado[k];
  }
  }
  candidato_encontrado = 0;

    cout << "\ndigite (1) - Votar em um Presidente\n";
    cout << "digite (2) - Votar em um branco\n";
    cout << "digite (3) - Anular o voto\n" << "Opcao:";
    cin >> tipo_de_voto;
    cout << "\n";

    if (tipo_de_voto == 2)
    {
    Urna::numero_de_votos_brancos++;
    }else

    if (tipo_de_voto == 3)
    {
    Urna::numero_de_votos_nulos++;
    }else

    if (tipo_de_voto == 1)
    {

    do
    {
      cout << "\nDigite o codigo de seu Presidente (2 digitos): ";
      cin >> presidente;

      do
      {
        if (presidente.size() != 2 )
        {
        cout << "\nCodigo para presidente não encontrado, informe um codigo valido:\n";
        cin >> presidente;
        }
        k = 0;
        for (int i = 0 ; i < Urna::candidato_Registrado.size(); i++)
        {
          if (presidente == Urna::candidato_Registrado[i].getNumero() )
          {
            if (Urna::candidato_Registrado[i].getCargo() == "PRESIDENTE" )
              {
              cout << "\nCandidato encontrado : \n";
              cout <<"UF      : "<<Urna::candidato_Registrado[i].getEstado()  << "\n";
              cout <<"Cargo   : "<<Urna::candidato_Registrado[i].getCargo()   << "\n";
              cout <<"Numero  : "<<Urna::candidato_Registrado[i].getNumero()  << "\n";
              cout <<"Nome    : "<<Urna::candidato_Registrado[i].getNomeUrna()<< "\n";
              cout <<"Partido : "<<Urna::candidato_Registrado[i].getPartido() << "\n";
              candidato_encontrado = 1;
	      k = i;
              }
          }
        }
      }while( (presidente.size() != 2));

    }while(candidato_encontrado == 0);

  voto_local[ii].set_nome_presidente(candidato_Registrado[k].getNomeUrna());

  candidato_Registrado[k].setVoto();

  if (candidato_Registrado[k].getVotos() > vencedor_presidente.getVotos())
  {
  vencedor_presidente = candidato_Registrado[k];
  }

  }

  Urna::votos_registrados.push_back(voto_local[ii]);

  }

}

void Urna::apresentaResultados()
{
  cout << "Votos em branco : "        << Urna::numero_de_votos_brancos << "\n";
  cout << "Votos nulos : "            << Urna::numero_de_votos_nulos << "\n\n";
  cout << "\n\nPresidente         : ";

  if (Urna::vencedor_presidente.getVotos() != 0)
  {
    cout << Urna::vencedor_presidente.getNomeUrna();
  }else{
    cout << "(Segundo Turno)";
  }

  cout << "\nSenador            : ";//   << Urna::vencedor_senador.getNomeUrna();
  if (Urna::vencedor_senador.getVotos() != 0)
  {
    cout << Urna::vencedor_senador.getNomeUrna();
  }else{
    cout << "(Segundo Turno)";
  }

  cout << "\nGovernador         : ";//   << Urna::vencedor_governador.getNomeUrna();
  if (Urna::vencedor_governador.getVotos() != 0)
  {
    cout << Urna::vencedor_governador.getNomeUrna();
  }else{
    cout << "(Segundo Turno)";
  }

  cout << "\nDeputado distrital : ";//   << Urna::vencedor_deputado_distrital.getNomeUrna();
  if (Urna::vencedor_deputado_distrital.getVotos() != 0)
  {
    cout << Urna::vencedor_deputado_distrital.getNomeUrna();
  }else{
    cout << "(Segundo Turno)";
  }

  cout << "\nDeputado federal   : ";//   << Urna::vencedor_deputado_federal.getNomeUrna();
  if (Urna::vencedor_deputado_federal.getVotos() != 0)
  {
    cout << Urna::vencedor_deputado_federal.getNomeUrna();
  }else{
    cout << "(Segundo Turno)";
  }

  cout << "\n";
}

void Urna::buscaCandidato(string codigo_do_candidato)
{
int k = 0;
  for (int i = 0 ; i < Urna::candidato_Registrado.size(); i++){
    if (codigo_do_candidato == Urna::candidato_Registrado[i].getNumero())
    {
    cout << "Candidato encontrado\n\n";
    cout << "\nUnidade da federacao : " << this->candidato_Registrado[i].getEstado();
    cout << "\nCargo pretendido :     " << this->candidato_Registrado[i].getCargo();
    cout << "\nNumero do candidato :  " << this->candidato_Registrado[i].getNumero();
    cout << "\nNome do candidato :    " << this->candidato_Registrado[i].getNomeUrna();
    cout << "\nNome do partido :      " << this->candidato_Registrado[i].getPartido();
    cout << "\nVotos recebidos :      " << this -> candidato_Registrado[i].getVotos();
    cout <<"\n";
    k = i;
    }
  }
}

void Urna::lista_votos()
{
cout << "\n\nlistagem de votos: \n";

  for (int i =0 ;i < Urna::votos_registrados.size(); i++)
  {
  cout <<"\n\nEleitor(a)             : "<< Urna::votos_registrados[i].get_nome_eleitor()          <<"\n";
  cout <<"Voto para Dep.federal  : ";//<< Urna::votos_registrados[i].get_nome_deputado_federal()   <<"\n";
  if (Urna::votos_registrados[i].get_nome_deputado_federal() == "")
  {
  cout << "(voto branco ou nulo)";
  }else{
  cout << Urna::votos_registrados[i].get_nome_deputado_federal();
  }
  cout <<"\nVoto para Dep.distrital: ";//<< Urna::votos_registrados[i].get_nome_deputado_distrital() <<"\n";
  if (Urna::votos_registrados[i].get_nome_deputado_distrital() == "")
  {
  cout << "(voto branco ou nulo)";
  }else{
  cout << Urna::votos_registrados[i].get_nome_deputado_distrital();
  }
  cout <<"\nVoto para Governador   : ";//<< Urna::votos_registrados[i].get_nome_governador()         <<"\n";
    if (Urna::votos_registrados[i].get_nome_governador() == "")
  {
  cout << "(voto branco ou nulo)";
  }else{
  cout << Urna::votos_registrados[i].get_nome_governador();
  }

  cout <<"\nVoto para Senador      : ";//<< Urna::votos_registrados[i].get_nome_senador()            <<"\n";
    if (Urna::votos_registrados[i].get_nome_senador() == "")
  {
  cout << "(voto branco ou nulo)";
  }else{
  cout << Urna::votos_registrados[i].get_nome_senador();
  }

  cout <<"\nVoto para Presidente   : ";//<< Urna::votos_registrados[i].get_nome_presidente()         <<"\n\n";
    if (Urna::votos_registrados[i].get_nome_presidente() == "")
  {
  cout << "(voto branco ou nulo)";
  }else{
  cout << Urna::votos_registrados[i].get_nome_presidente();
  }

  }

}
