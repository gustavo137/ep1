# EP 1 - OO-FGA

## Instruções para compilar o programa:

* Compile e execute o projeto com os comandos:

```sh
$ make
$ make run
```

## Funcionalidades do projeto

### Inicia Votação:

<p>Essa funcionalidade é a base para a execução das demais funcionalidades do projeto, nela são registrados os candidatos e o loop de votação</p>

### Apresentar Resultados:

<p>Na apresentação de resultados o programa exibe os vencedores de cada tipo de candidatura, e caso ocorra necessidade de segundo turno, uma mensagem com essa informação será exibida</p>

### Buscar Candidato:

<p> Após selecionada essa opção a urna recebe um código e busca todos os candidatos que tenham como código de voto ou partido o numero forncido, no caso de um presidente por exemplo, o programa imprime o presidente e o vice </p>

### Mostrar relatório de votos:

<p>Apresenta quais eleitores votaram em quais candidatos, ou se votaram em branco ou nulo</p>

## Bugs e problemas

<p>É de suma importância que o programa seja usado corretamente, por exemplo registrar mais de 2 nomes quando o nome do usuário é pedido pode gerar um loop infinito</p>

## Referências
