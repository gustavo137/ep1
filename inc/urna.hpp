#ifndef URNA_HPP
#define URNA_HPP

#include "candidatos.hpp"
#include "eleitor.hpp"
#include "pessoa.hpp"
#include "voto.hpp"

#include <string>
#include <bits/stdc++.h>

using namespace std;

class Urna{
public:

	Urna();
        ~Urna();

	vector<Candidato> candidato_Registrado;
	vector<Voto> votos_registrados;

	int numero_de_candidatos = 0;
        int numero_de_votos_nulos = 0;
        int numero_de_votos_brancos = 0;
	int getNumeroCandidatos();

	Candidato vencedor_presidente;
        Candidato vencedor_senador;
        Candidato vencedor_governador;
        Candidato vencedor_deputado_distrital;
        Candidato vencedor_deputado_federal;

        void setNumeroCandidatos(string path_to_file);
        void buscaCandidato(string codigo_do_candidato);
	void buscarInformacaoCandidato(); //recebe um codigo e retorna as informações do candidato respectivo ao código fornecido
	void iniciaVotacao();
	void registraCandidato(string path_to_file);
	void registraVoto();
	void apresentaResultados();
	void lista_votos();

private:

};

#endif
