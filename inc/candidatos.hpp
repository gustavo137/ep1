#ifndef CANDIDATOS_HPP
#define CANDIDATOS_HPP

#include "pessoa.hpp"

#include <bits/stdc++.h>
#include <string>

using namespace std;
class Candidato:public Pessoa{
private:
	string nm_ue;              //estado
        string ds_cargo;           //cargo
        string nr_candidato;       //numero candidato
        string nm_urna_candidato;  //nome na urna
        string nm_partido;         //partido
	long long int votos;

public:
	Candidato();
	Candidato(string nm_ue,
	string ds_cargo,
        string nr_candidato,
        string nm_urna_candidato,
        string nm_partido,
        long long int votos

	);
	//Get Methods
	string getEstado();
	string getCargo();
	string getNumero();
	string getNomeUrna();
	string getPartido();
	long long int getVotos();
	//Set Methods
	void setEstado(string estado);
        void setCargo(string cargo);
        void setNumero(string numero);
        void setNomeUrna(string nome);
        void setPartido(string partido);
        void setVoto();

};

#endif
