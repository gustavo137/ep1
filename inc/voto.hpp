#ifndef VOTO_HPP
#define VOTO_HPP

#include <bits/stdc++.h>
#include <string>

using namespace std;
class Voto{
private:
        string nome_eleitor;
        string nome_deputado_federal;
	string nome_deputado_distrital;
	string nome_governador;
	string nome_senador;
	string nome_presidente;
public:
        Voto();//constructor
        Voto(string nome_eleitor,
             string nome_deputado_federal,
             string nome_deputado_distrital,
	     string nome_governador,
             string nome_senador,
             string nome_presidente);

        //Get Methods
        string get_nome_eleitor();
        string get_nome_deputado_federal();
        string get_nome_deputado_distrital();
	string get_nome_governador();
        string get_nome_senador();
        string get_nome_presidente();

        //Set Methods
        void set_nome_eleitor            (string eleitor);
        void set_nome_deputado_federal   (string deputado_federal);
        void set_nome_deputado_distrital (string deputado_distrital);
	void set_nome_governador         (string governador);
        void set_nome_senador            (string senador);
        void set_nome_presidente         (string presidente);
};

#endif




